# TEditor

Minimalist vim-like text editor.

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```

## Usage

```bash
$ ./te [FILE]
```

## Keyboard shortcuts

|   Command    |Shortcut |
|--------------|---------|
| Copy line    |  ESC+y  |
| Paste line   |  ESC+p  |
| Paste before |  ESC+P  |
| Delete line  |  ESC+d  |
| Write file   |  ESC+w  |
| Quit         |  ESC+q  |
