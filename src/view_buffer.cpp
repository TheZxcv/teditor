#include "view_buffer.h"

ViewBuffer::ViewBuffer(const Console * const con, std::istream* stream) : console{con}, row_offset(0), cur{0,0} {
	if (stream != nullptr) {
		std::string line;
		while (std::getline(*stream, line)) {
			buffer.push_back(line);
		}
		if (buffer.size() == 0)
			buffer.push_back("");
	} else {
		buffer.push_back("");
	}
}

ViewBuffer::~ViewBuffer() {}
