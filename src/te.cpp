#include "te.h"
#include "console.h"
#include "cursor.h"
#include "view_buffer.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

const std::string newline{"\r\n"};
const char ESCAPE    = 27;
const char BACKSPACE = 127;
const char UP        = 65;
const char DOWN      = 66;
const char RIGHT     = 67;
const char LEFT      = 68;

const char* opened_file = "(none)";
bool saved = false;

void render_status_bar(Console& con, const Cursor& cur) {
	std::ostringstream oss{};
	oss << "\x1b[32m[TEditor]\x1b[0m " << opened_file << ": " << cur.y << "," << cur.x;
	if (saved) {
		oss << "\x1b[7mSAVED\x1b[0m";
		saved = !saved;
	}
	con.write(oss.str());
}

void render(Console& con, const ViewBuffer& buffer) {
	con.clear_screen();
	con.move_cursor(0, 0);
	int nlines = buffer.render({ [&con](auto line) {
			con.write(line);
			con.write(newline);
	}});

	for (; nlines < con.get_view_lines(); nlines++) {
		con.write("\x1b[2m~\x1b[0m");
		con.write(newline);
	}

	render_status_bar(con, buffer.get_position());
	con.move_cursor(buffer.get_cursor());
}

static std::string copy_buffer{};
static bool handle_input(Console& con, ViewBuffer& buffer) {
	int n = con.read();
	if (n == EOF) {
		return false;
	}

	char ch = (char) n;
	switch(ch) {
		case ESCAPE:
			ch = con.read();
			if (ch == '[') { // ANSI Code
				ch = con.read();
				switch (ch) {
				case UP: buffer.move_up();
					break;
				case DOWN: buffer.move_down();
					break;
				case LEFT: buffer.move_left();
					break;
				case RIGHT: buffer.move_right();
					break;
				default:
					// do nothing
					break;
				}
			} else if (ch == 'q') { // quit
				return false;
			} else if (ch == 'y') {
				copy_buffer = buffer.current_line();
			} else if (ch == 'P') {
				buffer.insert_line_above_current(copy_buffer);
			} else if (ch == 'p') {
				buffer.insert_line_below_current(copy_buffer);
			} else if (ch == 'd') {
				copy_buffer = buffer.delete_current_line();
			} else if (ch == 'w') {
				std::fstream file{};
				file.open(opened_file, std::ios::out);
				if (file.good()) {
					buffer.write(file);
					saved = true;
					file.close();
				}
			}
			break;

		case BACKSPACE:
			buffer.delete_current();
			break;

		case '\r':
			buffer.insert_newline_on_current();
			break;

		default:
			buffer.insert_char_on_current(ch);
			break;
	}

	return true;
}

ViewBuffer create_buffer(const Console* const con, const char* const filename) {
	if (filename != nullptr) {
		std::fstream file{};
		file.open(filename, std::ios::in);
		if (file.good()) {
			ViewBuffer buffer{con, &file};
			file.close();
			return buffer;
		} else {
			exit(EXIT_FAILURE);
		}
	} else {
		return ViewBuffer{con};
	}
}

int main(int argc, char *argv[]) {
	Console con{std::cin, std::cout};

	const char* filename = nullptr;
	if (argc > 1) {
		filename = argv[1];
		opened_file = filename;
	}
	ViewBuffer buffer = create_buffer(&con, filename);

	do {
		render(con, buffer);
	} while (handle_input(con, buffer));
	con.clear_screen();
	return 0;
}
