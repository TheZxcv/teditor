#include "console.h"

#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

static struct termios bak, settings;

void Console::clear_screen() {
	m_out << "\x1b[2J";
}

void Console::move_cursor(int x, int y) {
	m_out << "\x1b[" << (y + 1) << ";" << (x + 1) << "H";
}

void Console::move_cursor(const Cursor& cur) {
	move_cursor(cur.x, cur.y);
}

int Console::read() {
	return m_in.get();
}

void Console::write(const std::string& str) {
	m_out << str;
}

int Console::get_view_lines() const {
	return nrows - 1;
}

void Console::init_term() {
	// switch to alternate buffer mode
	m_out << "\x1b[?1049h\x1b[H";

	tcgetattr(STDOUT_FILENO, &bak);
	settings = bak;
	settings.c_lflag &= ~ICANON; /* disable buffered i/o */
	settings.c_lflag &= ~ECHO;   /* disable echo mode */
	/* input modes - clear indicated ones giving: no break, no CR to NL,
	no parity check, no strip char, no start/stop output (sic) control */
	settings.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);

	/* output modes - clear giving: no post processing such as NL to CR+NL */
	settings.c_oflag &= ~(OPOST);

	/* control modes - set 8 bit chars */
	settings.c_cflag |= (CS8);

	/* local modes - clear giving: echoing off, canonical off (no erase with
	backspace, ^U,...),  no extended functions, no signal chars (^Z,^C) */
	settings.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
	cfmakeraw(&settings);
	tcsetattr(STDOUT_FILENO, TCSANOW, &settings);

	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	nrows = w.ws_row;
	ncols = w.ws_col;
}


void Console::restore_term() {
	tcsetattr(0, TCSANOW, &bak);
	// exit alternate buffer mode
	m_out << "\x1b[?1049l";
}
