#ifndef CONSOLE_H
#define CONSOLE_H

#include "cursor.h"

#include <istream>
#include <ostream>

class Console {
public:
	Console(std::istream& in, std::ostream& out) : m_in(in), m_out(out) {
		init_term();
	};

	~Console() {
		restore_term();
	};

	void clear_screen();
	void move_cursor(int x, int y);
	void move_cursor(const Cursor&);

	int read();
	void write(const std::string&);

	int get_view_lines() const;

	unsigned short ncols;
	unsigned short nrows;

private:
	std::istream& m_in;
	std::ostream& m_out;

	void init_term();
	void restore_term();
};

#endif

