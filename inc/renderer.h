#ifndef RENDERER_H
#define RENDERER_H

#include <functional>

struct Renderer {
	std::function<void(const std::string&)> render;
};

#endif
