#ifndef VIEW_BUFFER_H
#define VIEW_BUFFER_H

#include "console.h"
#include "cursor.h"
#include "renderer.h"

#include <istream>
#include <vector>

class ViewBuffer {
private:
	const Console* const console;
	int row_offset;
	Cursor cur;
	std::vector<std::string> buffer;

public:
	ViewBuffer(const Console* const con, std::istream* stream = nullptr);
	~ViewBuffer();

	void insert_char_on_current(char ch) {
		buffer[cur.y].insert(cur.x, 1, ch);
		cur.x++;
		cur.clamp(buffer);
	}

	void insert_newline_on_current() {
		auto next_line = std::next(std::begin(buffer), cur.y+1);
		auto sub = buffer[cur.y].substr(cur.x);
		buffer.insert(next_line, sub);
		buffer[cur.y].erase(cur.x);
		cur.y++;
		cur.x = 0;
		if (cur.y - row_offset >= console->get_view_lines()) {
			row_offset++;
		}
	}

	void delete_current() {
		if (cur.x == 0) {
			if (cur.y != 0) {
				std::string copy = buffer[cur.y];
				auto current = std::next(std::begin(buffer), cur.y);
				buffer.erase(current);
				cur.x = buffer[cur.y - 1].size();
				buffer[cur.y - 1].append(copy);
				cur.y--;
				if (cur.y < row_offset) {
					row_offset--;
				}
				cur.clamp(buffer);
			}
		} else {
			buffer[cur.y].erase(buffer[cur.y].begin() + cur.x - 1);
			cur.x--;
			cur.clamp(buffer);
		}
	}

	std::string current_line() const {
		return buffer[cur.y];
	}

	std::string delete_current_line() {
		auto current = std::next(std::begin(buffer), cur.y);
		auto deleted_line = *current;
		buffer.erase(current);
		cur.clamp(buffer);
		return deleted_line;
	}

	void insert_line_below_current(const std::string& line) {
		cur.y++;
		auto current = std::next(std::begin(buffer), cur.y);
		buffer.insert(current, line);
	}

	void insert_line_above_current(const std::string& line) {
		auto current = std::next(std::begin(buffer), cur.y);
		buffer.insert(current, line);
	}

	Cursor get_cursor() const {
		return {cur.x, cur.y - row_offset};
	}

	Cursor get_position() const {
		return {cur.x + 1, cur.y + 1};
	}

	int render(const Renderer& renderer) const {
		int nlines = 0;
		auto line = std::next(std::begin(buffer), row_offset);
		for (; line != std::end(buffer) && nlines < console->get_view_lines(); line++, nlines++) {
			renderer.render(*line);
		}
		return nlines;
	}

	void move_up() {
		if (cur.y == row_offset && row_offset > 0)
			row_offset--;
		cur.y--;
		cur.clamp(buffer);
	}

	void move_down() {
		cur.y++;
		if (cur.y - row_offset == console->get_view_lines() && cur.y < (int)buffer.size())
			row_offset++;
		cur.clamp(buffer);
	}

	void move_left() {
		cur.x--;
		cur.clamp(buffer);
	}

	void move_right() {
		cur.x++;
		cur.clamp(buffer);
	}

	bool write(std::ostream& output) {
		for (const std::string& line : buffer) {
			output << line << std::endl;
		}
		return true;
	}
};

#endif
