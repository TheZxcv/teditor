#ifndef CURSOR_H
#define CURSOR_H

#include <string>
#include <vector>

struct Cursor {
	int x;
	int y;

	void clamp(const std::vector<std::string>& buffer) {
		if (y >= (int) buffer.size())
			y = ((int) buffer.size()) - 1;
		if (y < 0)
			y = 0;

		if (x < 0)
			x = 0;
		if (x > (int) buffer[y].size())
			x = (int) buffer[y].size();
	}
};

#endif
